load("cirrus", "env", "http")

def execute(query, variables=None):
    body = {
        "query": query,
        "variables": variables or {}
    }

    headers = {}
    if "GITLAB_TOKEN" in env:
        headers["Authorization"] = "Bearer " + env["GITLAB_TOKEN"]

    response = http.post(
        url=env.get("X-Gitlab-Instance", "https://gitlab.com") + "/api/graphql",
        headers=headers,
        json_body=body
    )

    if response.status_code != 200:
        print("Bad response code {}".format(response.status_code))
        print(response.body())
        fail("GraphQL call got bad response code {}".format(response.status_code))

    jsonResponse = response.json()

    if ("errors" in jsonResponse) and len(jsonResponse["errors"]) > 0:
        fail("GraphQL query returned errors {}".format(jsonResponse["errors"]))

    return jsonResponse["data"]
