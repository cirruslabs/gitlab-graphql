# Helpers to interact with GraphQL API from GitLab Actions

```python
load("gitlab.com/cirruslabs/gitlab-graphql.git", "execute")

def findAffectedFiles(projectId, mergeRequestId):
    variables = {
        "projectFullPath": projectId,
        "mergeRequestId": str(mergeRequestId),
    }
    queryData = execute("""
    query($projectFullPath: ID!, $mergeRequestId: String!) {
      project(fullPath: $projectFullPath) {
      	mergeRequest(iid: $mergeRequestId) {
          diffStats {
            path
          }
        }
      }
    }
    """, variables=variables)

    return [diffStat["path"] for diffStat in queryData["project"]["mergeRequest"]["diffStats"]]
```

